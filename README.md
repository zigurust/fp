# Functional Programming in Zig

This repository demonstrates some examples of functional programming style in Zig.

## Compiler version

Zig 0.11.0

## Further reading

* [Introduction to monads](https://www.youtube.com/watch?app=desktop&v=C2w45qRc3aU)
* [Currying vs. partial evaluation](https://betterprogramming.pub/functional-programming-currying-vs-partial-application-53b8b05c73e3)
* [Currying != partial evaluation](http://lambda-the-ultimate.org/node/2266)
* [Difference currying vs. partial evaluation](https://stackoverflow.com/questions/218025/what-is-the-difference-between-currying-and-partial-application)
* [Why isn't functional programming the norm](https://www.youtube.com/watch?v=QyJZzq0v7Z4)
* [OOP is bad](https://www.youtube.com/watch?app=desktop&v=QM1iUe6IofM)
* [OOP vs FP](https://www.youtube.com/watch?app=desktop&v=wyABTfR9UTU)
* [FP principes in JS](https://www.freecodecamp.org/news/functional-programming-principles-in-javascript-1b8fc6c3563f)
* [Imperative vs. declarative](https://ui.dev/imperative-vs-declarative-programming)
* [Shared mutable state is the root of all evil](http://henrikeichenhardt.blogspot.com/2013/06/why-shared-mutable-state-is-root-of-all.html?m=1)
* [Overview of FP](https://www.cloudbees.com/blog/overview-of-functional-programming)
* [Master the JS interview What is functional programming?](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-functional-programming-7f218c68b3a0)
* [Functional programming jargon](https://github.com/hemanth/functional-programming-jargon)
* [ramda](https://github.com/ramda/ramda)
* [folktale](https://folktale.origamitower.com/)
* [Elevate Your Rust Code: The Art of Separating Actions and Calculations](https://rusty-ferris.pages.dev/blog/fp-actions-vs-calculations/)

