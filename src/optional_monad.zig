pub fn somev1(comptime T: type, t: T) ?T {
    const optional_value: ?T = t;
    return optional_value;
}

test "somev1" {
    // Arrange
    var value: u8 = 5;
    // Act
    var optional_value = somev1(u8, value);
    // Assert
    const expect = @import("std").testing.expect;
    const expectEqual = @import("std").testing.expectEqual;
    if (optional_value) |v| {
        try expect(true);
        try expectEqual(v, 5);
    } else {
        try expect(false);
    }
}

pub fn somev2(t: anytype) ?@TypeOf(t) {
    const optional_value: ?@TypeOf(t) = t;
    return optional_value;
}

test "somev2" {
    // Arrange
    var value: u8 = 5;
    // Act
    var optional_value = somev2(value);
    // Assert
    const expect = @import("std").testing.expect;
    const expectEqual = @import("std").testing.expectEqual;
    if (optional_value) |v| {
        try expect(true);
        try expectEqual(v, 5);
    } else {
        try expect(false);
    }
}

pub fn runv1(comptime T: type, comptime V: type, input: ?T, transform: fn (value: T) ?V) ?V {
    if (input) |value| {
        return transform(value);
    } else {
        return null;
    }
}

test "runv1" {
    // Arrange
    const expect = @import("std").testing.expect;
    const expectEqual = @import("std").testing.expectEqual;
    const transform = struct {
        fn func(value: u8) ?u16 {
            var ret: ?u16 = null;
            if (value < 100) {
                ret = @as(u16, value) * @as(u16, value);
            }
            return ret;
        }
    }.func;
    // Act
    const value1 = runv1(u8, u16, 50, transform);
    const value2 = runv1(u8, u16, 100, transform);
    // Assert
    if (value1) |v| {
        try expect(true);
        try expectEqual(v, 2500);
    } else {
        try expect(false);
    }
    if (value2) |_| {
        try expect(false);
    } else {
        try expect(true);
    }
}

inline fn FunctionReturnType(comptime Function: anytype) type {
    const info = @typeInfo(@TypeOf(Function));
    if (info != .Fn) {
        @compileError("Only functions can be instrumented");
    }
    const return_type = info.Fn.return_type orelse @compileError("Null return type is not supported");
    return return_type;
}

pub fn map(input: anytype, transform: anytype) FunctionReturnType(transform) {
    if (input) |value| {
        return transform(value);
    } else {
        return null;
    }
}

test "map" {
    // Arrange
    const expect = @import("std").testing.expect;
    const expectEqual = @import("std").testing.expectEqual;
    const transform = struct {
        fn func(value: u8) ?u16 {
            var ret: ?u16 = null;
            if (value < 100) {
                ret = @as(u16, value) * @as(u16, value);
            }
            return ret;
        }
    }.func;
    const param2: ?u8 = 100;
    // Act
    const value1 = map(@as(?u8, 50), transform);
    const value2 = map(param2, transform);
    const value3 = map(null, transform);
    // Assert
    if (value1) |v| {
        try expect(true);
        try expectEqual(v, 2500);
    } else {
        try expect(false);
    }
    if (value2) |_| {
        try expect(false);
    } else {
        try expect(true);
    }
    if (value3) |_| {
        try expect(false);
    } else {
        try expect(true);
    }
}

fn FunctionParameter(comptime f: anytype, comptime parameter_number: comptime_int) type {
    const yInfo = @typeInfo(@TypeOf(f));
    if (yInfo != .Fn) {
        @compileError("Only for a function can the argument be extracted");
    }
    return yInfo.Fn.params[parameter_number].type orelse @compileError("Function must have a parameter with given parameter_number.");
}

pub fn mapv2(transform: anytype, input: ?FunctionParameter(transform, 0)) FunctionReturnType(transform) {
    if (input) |value| {
        return transform(value);
    } else {
        return null;
    }
}

test "mapv2" {
    // Arrange
    const expect = @import("std").testing.expect;
    const expectEqual = @import("std").testing.expectEqual;
    const transform = struct {
        fn func(value: u8) ?u16 {
            var ret: ?u16 = null;
            if (value < 100) {
                ret = @as(u16, value) * @as(u16, value);
            }
            return ret;
        }
    }.func;
    // Act
    const value1 = mapv2(transform, 50);
    const value2 = mapv2(transform, 100);
    const value3 = mapv2(transform, null);
    // Assert
    if (value1) |v| {
        try expect(true);
        try expectEqual(v, 2500);
    } else {
        try expect(false);
    }
    if (value2) |_| {
        try expect(false);
    } else {
        try expect(true);
    }
    if (value3) |_| {
        try expect(false);
    } else {
        try expect(true);
    }
}
