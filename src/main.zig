const std = @import("std");

// pure functions
fn circleArea(radius: f64, pi: f64) f64 {
    return radius * radius * pi;
}

// immutability

fn addOne(comptime N: comptime_int, from: [N]u8) [N]u8 {
    var to = [_]u8{0} ** N;
    for (from, &to) |from_value, *to_value| {
        to_value.* = from_value + 1;
    }
    return to;
}

test "addOne" {
    // Arrange
    const from = [_]u8{ 0, 1, 2, 3 };
    // Act
    const to = addOne(4, from);
    // Assert
    const expected = [_]u8{ 1, 2, 3, 4 };
    try std.testing.expectEqualSlices(u8, &expected, &to);
}

fn reduce(comptime FromType: type, comptime N: comptime_int, comptime ToType: type, initial: ToType, kernel: fn (from_value: *const FromType, accumulator: *const ToType) ToType, from: [N]FromType) ToType {
    var accumulator = initial;
    for (&from) |*value| {
        accumulator = kernel(value, &accumulator);
    }
    return accumulator;
}

test "reduce" {
    // Arrange
    const from = [_]u8{ 1, 2, 3, 4 };
    const kernel = struct {
        fn sum(from_value: *const u8, accumulator: *const u8) u8 {
            return from_value.* + accumulator.*;
        }
    }.sum;
    // Act
    const sum = reduce(u8, 4, u8, 0, kernel, from);
    // Assert
    try std.testing.expectEqual(sum, 10);
}

test "chaining" {
    // Arrange
    const to = [_]u8{ 0, 1, 2, 3 };
    const kernel = struct {
        fn sum(from_value: *const u8, accumulator: *const u8) u8 {
            return from_value.* + accumulator.*;
        }
    }.sum;
    // Act
    const sum = reduce(u8, 4, u8, 0, kernel, addOne(4, addOne(4, to)));
    // Assert
    try std.testing.expectEqual(sum, 14);
}

// higher-order functions

// partial evaluation

fn partialV1(comptime Param1: type, comptime Param2: type, comptime ReturnType: type, comptime f: fn (param1: Param1, param2: Param2) ReturnType, param1: Param1) fn (param2: Param2) ReturnType {
    return struct {
        fn partialiedFunction(param2: Param2) ReturnType {
            return f(param1, param2);
        }
    }.partialiedFunction;
}

test "partialV1" {
    // Arrange
    const add = struct {
        fn func(a: u8, b: u8) u8 {
            return a + b;
        }
    }.func;
    const addTo2 = partialV1(u8, u8, u8, add, 2);
    // Act
    var value = addTo2(5);
    // Assert
    try std.testing.expectEqual(value, 7);
}

fn ParamType(comptime f: anytype, comptime parameter_number: comptime_int) type {
    const yInfo = @typeInfo(@TypeOf(f));
    if (yInfo != .Fn) {
        @compileError("Only for a function can the argument be extracted");
    }
    return yInfo.Fn.params[parameter_number].type orelse @compileError("Function must have a single parameter.");
}

fn partialV2(comptime f: anytype, a: ParamType(f, 0)) fn (ParamType(f, 1)) functionReturnType(f) {
    return struct {
        fn partialiedFunction(b: ParamType(f, 1)) functionReturnType(f) {
            return f(a, b);
        }
    }.partialiedFunction;
}

test "partialV2" {
    // Arrange
    const add = struct {
        fn func(a: u8, b: u8) u8 {
            return a + b;
        }
    }.func;
    const addTo2 = partialV2(add, 2);
    // Act
    var value = addTo2(5);
    // Assert
    try std.testing.expectEqual(value, 7);
}

// composition

fn getParametersOfFunctions(comptime functions: anytype) [functions.len]std.builtin.Type.Fn.Param {
    var parameters: [functions.len]std.builtin.Type.Fn.Param = undefined;

    for (functions, 0..) |function, i| {
        const functionTypeInfo = @typeInfo(@TypeOf(function));
        // one should obviously implement some checks before simply accessing this
        parameters[i] = functionTypeInfo.Fn.params[0];
    }

    return parameters;
}

fn getStructFieldsFromParameters(comptime N: comptime_int, parameterInfos: [N]std.builtin.Type.Fn.Param) [N]std.builtin.Type.StructField {
    var structFields: [N]std.builtin.Type.StructField = undefined;

    for (parameterInfos, 0..) |parameter, i| {
        const optional_type_info = std.builtin.Type.Optional{ .child = parameter.type orelse @compileError("All functions need to have a parameter with one type.") };
        const optional_type = @Type(std.builtin.Type{ .Optional = optional_type_info });
        const field_name = [1]u8{std.fmt.digitToChar(i, .upper)};
        const default_value: optional_type = null;
        const field = std.builtin.Type.StructField{
            .name = &field_name,
            .type = optional_type,
            .default_value = &default_value,
            .is_comptime = parameter.is_generic,
            .alignment = @alignOf(optional_type),
        };
        structFields[i] = field;
    }

    return structFields;
}

fn ComposedParameters(comptime functions: anytype) type {
    const parameterTypeInfos: [functions.len]std.builtin.Type.Fn.Param = getParametersOfFunctions(functions);
    const fields: [functions.len]std.builtin.Type.StructField = getStructFieldsFromParameters(functions.len, parameterTypeInfos);
    const declarations = [0]std.builtin.Type.Declaration{};
    const tupleInfo = std.builtin.Type.Struct{
        .fields = &fields,
        .is_tuple = true,
        .layout = .Auto,
        .decls = &declarations,
    };
    const typeInfo = std.builtin.Type{ .Struct = tupleInfo };
    const ParametersType = @Type(typeInfo);
    return ParametersType;
}

fn Compose(comptime functions: anytype) type {
    // obviously this method should check if all elements of the functions tuple are functions and that they can be composed.
    const number_of_functions = functions.len;
    const last_element = number_of_functions - 1;
    const ReturnTypeComposedFunction = functionReturnType(functions[last_element]);
    const ParameterTypeComposedFunction = ParamType(functions[0], 0);

    return struct {
        fn call(parameter: ParameterTypeComposedFunction) ReturnTypeComposedFunction {
            const ParametersType = ComposedParameters(functions);
            var return_values = ParametersType{};
            return_values[0] = parameter;
            inline for (functions, 0..) |function, i| {
                const return_value = @call(.auto, function, .{return_values[i].?});
                if (i == number_of_functions - 1) {
                    return return_value;
                } else {
                    return_values[i + 1] = return_value;
                }
            }
        }
    };
}

test "compose" {
    // Arrange
    const add2 = struct {
        fn func(a: u8) u8 {
            return a + 2;
        }
    }.func;
    const mul2 = struct {
        fn func(a: u8) u8 {
            return a * 2;
        }
    }.func;
    const comp = Compose(.{ add2, mul2 }).call; // 2*(x+2)
    // Act
    const value = comp(3);
    // Assert
    try std.testing.expectEqual(value, 10);
}

// currying

fn curry3v1(comptime P1: type, comptime P2: type, comptime P3: type, comptime R: type, function: fn (P1, P2, P3) R) fn (comptime P1) fn (comptime P2) fn (P3) R {
    const s1 = struct {
        fn f1(comptime p1: P1) fn (comptime P2) fn (P3) R {
            const s2 = struct {
                fn f2(comptime p2: P2) fn (P3) R {
                    const s3 = struct {
                        fn f3(p3: P3) R {
                            return function(p1, p2, p3);
                        }
                    };
                    return s3.f3;
                }
            };
            return s2.f2;
        }
    };
    return s1.f1;
}

test "curry3v1" {
    // Arrange
    const add3 = struct {
        fn func(a: u8, b: u8, c: u8) u8 {
            return a + b + c;
        }
    }.func;
    // Act
    var p3: u8 = 3;
    const f1 = curry3v1(u8, u8, u8, u8, add3);
    const f2 = f1(1);
    const f3 = f2(2);
    var value = f3(p3);
    value *= 2;
    const curriedFunction = curry3v1(u8, u8, u8, u8, add3);
    var value2 = curriedFunction(2)(4)(8);
    // Assert
    try std.testing.expectEqual(value, 12);
    try std.testing.expectEqual(value2, 14);
}

// combinators

fn wrongS(comptime x: anytype, comptime y: anytype) SReturnType(x, y) {
    return struct {
        fn func(z: SArgType(y)) functionReturnType(x) {
            return x(z, y(z));
        }
    }.func;
}

// fn s(comptime x: fn(functionReturnType(y), @TypeOf(z)) functionReturnType(x), comptime y: fn(@TypeOf(z)) functionReturnType(y), z: anytype) functionReturnType(x) {
// }

// fn s2(comptime Z:type, comptime y: fn(Z) functionReturnType(y), compttime x: fn(Z,functionReturnType(y)) functionReturnType(x)) functionReturnType(x) {
//     return x(z, y(z));
// }

fn SArgType(comptime y: anytype) type {
    const yInfo = @typeInfo(@TypeOf(y));
    if (yInfo != .Fn) {
        @compileError("Only for a function can the argument be extracted");
    }
    return yInfo.Fn.params[0].type orelse @compileError("Function must have a single parameter.");
}

fn SReturnType(comptime x: anytype, comptime y: anytype) type {
    const xInfo = @typeInfo(@TypeOf(x));
    const yInfo = @typeInfo(@TypeOf(y));
    if (xInfo != .Fn or yInfo != .Fn) {
        @compileError("Only functions can be combined with s combinator");
    }

    var functionInfo = std.builtin.Type{ .Fn = std.builtin.Type.Fn{
        .calling_convention = xInfo.Fn.calling_convention,
        .params = yInfo.Fn.params,
        .alignment = xInfo.Fn.alignment,
        .is_generic = xInfo.Fn.is_generic,
        .is_var_args = xInfo.Fn.is_var_args,
        .return_type = xInfo.Fn.return_type,
    } };

    return @Type(functionInfo);
}

inline fn functionReturnType(comptime Function: anytype) type {
    const info = @typeInfo(@TypeOf(Function));
    if (info != .Fn) {
        @compileError("Only functions can be instrumented");
    }
    const return_type = info.Fn.return_type orelse @compileError("Null return type is not supported");
    return return_type;
}

test "S combinator" {
    const functions = struct {
        fn double(value: u8) u8 {
            return 2 * value;
        }
        fn add(a: u8, b: u8) u8 {
            return a + b;
        }
    };

    const combined = wrongS(functions.add, functions.double);
    const value: u8 = combined(5);

    try std.testing.expectEqual(value, 15);
}

pub fn main() !void {}
